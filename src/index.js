import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Properties from './Components/properties'
import * as serviceWorker from './serviceWorker';

// console.log(properties);
ReactDOM.render(<Properties name={'sas'} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
