import React, { Component } from 'react';
import axios from 'axios';
import App from "../App";
import {BootstrapTable,
    TableHeaderColumn} from 'react-bootstrap-table';
class properties extends React.Component {
    state = {
        data: [],
        token: []
    };


    componentDidMount(prevProps, prevState, snapshot) {

        axios.get(`http://engine.algus.net/session/token`,
            {
                withCredentials: true,
            }
        )
            .then(res => {
                const token = res.data;
                console.log(token);
                this.setState({token: token})
                axios.post(`http://engine.algus.net/properties/add`,
                    {
                        withCredentials: true,
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-Token': this.state.token,
                        },
                    })
                    .then(res => {
                        console.log(res);
                    })
            })

        axios.get(`http://engine.algus.net/properties`,
            {
                withCredentials: true,
            }
        )
            .then(res => {
                console.log(res)
                const data = res.data;
                this.setState({data: data});
            })




    }


    render() {
        return (
            <BootstrapTable data={this.state.data}>
                <TableHeaderColumn isKey dataField='pid'>
                    ID
                </TableHeaderColumn>
                <TableHeaderColumn dataField='type'>
                    Тип данных
                </TableHeaderColumn>
                <TableHeaderColumn dataField='name'>
                    Машинное имя
                </TableHeaderColumn>
                <TableHeaderColumn dataField='label'>
                    Название поля
                </TableHeaderColumn>
                <TableHeaderColumn dataField='measure'>
                    Ед. измерения
                </TableHeaderColumn>
            </BootstrapTable>
        );
    }
}

export default properties;
